<?php namespace MiguelVillegas\Dishes\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateMiguelvillegasDishes extends Migration
{
    public function up()
    {
        Schema::create('miguelvillegas_dishes_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name', 200);
            $table->string('description', 500);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('miguelvillegas_dishes_');
    }
}
