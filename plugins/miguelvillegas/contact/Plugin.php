<?php namespace MiguelVillegas\Contact;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            'MiguelVillegas\Contact\Components\ContactForm'=> 'contactform'
        ];
    }

    public function registerSettings()
    {
    }
}
