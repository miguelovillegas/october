<?php 

namespace MiguelVillegas\Contact\Components;
 use Cms\Classes\ComponentBase;
 use Input;
 use Mail;
 use Validator;
 use Redirect;
 use Flash;


class ContactForm extends ComponentBase{

    public function componentDetails()
    {
        return [
            'name'=> 'Formulario de contacto',
            'description'=>'Formulario de contacto'
        ];
    }

    public function onSend(){
        $name = Input::get('name');
        $email = Input::get('email');
        $subject = Input::get('subject');
        $message = Input::get('message');

        $validate = Validator::make(
            [
                'name'=> $name,
                'email'=> $email,
                'message'=> $message,
                'subject'=> $subject
            ] ,
           [
            'name'=> 'required',
            'email'=> 'required|email',
            'message'=> 'required',
            'subject'=> 'required'
           ] ,
          [
            'required'=> 'El campo es requerido',
            'email'=> 'El email no es válido',
          ]
        );

        if($validate->fails()){
             return Redirect::back()->withErrors($validate);   
        }else{
            $vars = ['name'=> $name, 'email'=> $email, 'subject'=>$subject,'messageUser'=>$message];
            Mail::send('miguelvillegas.contact::mail.message',$vars, function($message){
                $message->to('pepe@zoneksystem.com','Administrador');
                $message->subject('Mensaje de cliente');
            });

            Flash::success('Tu mensaje ha sido enviado correctamente');
            return Redirect::back();
        }
    }


 }
