<?php namespace MiguelVillegas\Services\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateMiguelvillegasServices extends Migration
{
    public function up()
    {
        Schema::create('miguelvillegas_services_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 100);
            $table->text('description');
            $table->string('icon', 50);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('miguelvillegas_services_');
    }
}
