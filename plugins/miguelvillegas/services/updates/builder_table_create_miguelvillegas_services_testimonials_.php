<?php namespace MiguelVillegas\Services\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateMiguelvillegasServicesTestimonials extends Migration
{
    public function up()
    {
        Schema::create('miguelvillegas_services_testimonials_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('author', 100);
            $table->text('comment');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('miguelvillegas_services_testimonials_');
    }
}
