<?php return [
    'plugin' => [
        'name' => 'Servicio',
        'description' => 'Descripción',
        'clase'=> 'Clase Css para icono'
    ],
    'lang' => [
        'plugin' => [
            'success' => [
                'save' => 'Cambios guardados',
            ],
            'updated' => 'Cambio actualizado correctamente',
            'deleted' => 'Registro eliminado correctamente',
            'name' => 'Nombre',
            'description' => 'Descripción',
            'clase' => 'Clase css para icono',
        ],
    ],
];